#include<iostream>
#include<string>
#include<sstream>
#include <stdio.h>
#include "contpagetemp.h"

using namespace	std;


// ---------------------------------------------------
// file:	contpagetemp.cpp
// author:	Frode Klevstul (frode@klevstul.com)
// started:	20030611
// ---------------------------------------------------


Contpagetemp::Contpagetemp(){

	// define importance for this template
	_importance[1]=true;
	_importance[2]=true;
	_importance[3]=true;
	_importance[4]=true;
	_importance[5]=true;
	_importance[6]=true;
	_importance[7]=true;
	_importance[8]=true;
	_importance[9]=true;
	_importance[10]=true;

	// define the number of	columns for this template
	_nocolumns = 5;
}


Contpagetemp::~Contpagetemp(){}



string Contpagetemp::fetchArticles(int pagewidth, int pageheight, int pageno, Articlehandler* articlehandler) const{

	return fetchArticlesMethod(pagewidth, pageheight, pageno, _nocolumns, _importance, articlehandler,0);

}
