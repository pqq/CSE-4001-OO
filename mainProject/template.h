#ifndef TEMPLATE_H_PROTECTOR
#define TEMPLATE_H_PROTECTOR

//#include <iostream>
//#include "exporter.h"
#include "articlehandler.h"
using namespace std;


// ---------------------------------------------------
// file:	template.h
// author:	Frode Klevstul (frode@klevstul.com)
// started:	20030609
// ---------------------------------------------------

class Template
{
    public:

		static Template* getTemplate(const string& name);

		virtual string fetchArticles(int pagewidth, int pageheight, int pageno, Articlehandler* articlehandler) const = 0;
		
		string fetchArticlesMethod(int pagewidth, int pageheight, int pageno, int nocolumns, const bool importance [10], Articlehandler* articlehandler, int maxsize) const;

};


#endif
