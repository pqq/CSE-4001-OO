#ifndef EXPORT2HTML_H_PROTECTOR
#define EXPORT2HTML_H_PROTECTOR

#include <iostream>
#include "exporteralgorithm.h"
using namespace std;


// ---------------------------------------------------
// file:	export2html.h
// author:	Frode Klevstul (frode@klevstul.com)
// started:	20030606
// ---------------------------------------------------

class Export2HTML : public ExporterAlgorithm
{
    public:

		Export2HTML();

		virtual ~Export2HTML();
	
		string parseContent(string content, int pagewdith, int pageheight, bool showoutput) const;

		bool createOutputfile(string filename, string content) const;

	private:
	
		string substituteTags(string page, int pagewidth, int pageheight, int colno, int pageno) const;
		
		string getArticleno(string article) const;
		
		string getTitle(string article) const;

};


#endif
