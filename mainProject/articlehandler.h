#ifndef ARTICLEHANDLER_H
#define ARTICLEHANDLER_H
#include <iostream>
#include <cstring>
#include <cassert>
#include <string>
#include "article.h"
using namespace std;

class Articlehandler{

 public:
	Articlehandler();
	Articlehandler(Articlehandler* articlehandler);
	void setArticles();
	int getSize();
	void printStatus();
	int getStatus();
 public:      
        Article *articles[100];
       
};
#endif
