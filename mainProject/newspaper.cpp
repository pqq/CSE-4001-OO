#include "newspaper.h"
#include <iostream>
#include <string>
using namespace std;

Newspaper::Newspaper(int choice)
{
	name = "Picture Perfect";
	noPages = 0;
	if (choice == 1)
	{
		pageWidth = 80;
		pageHeight = 50;
	}
	else if (choice == 2)
	{
		cout<<"Enter Page width (in bytes) : ";
		cin>>pageWidth;
		cout<<"Enter Page height (in no. of line) : ";
		cin>>pageHeight;
	}
//	pageWidth = 80;		// # of bytes
//	pageHeight = 50;	// # of lines
	pageContent = "";

	ahObj = new Articlehandler();	
}

Newspaper::~Newspaper()
{
	delete ahObj;

}


int Newspaper::autoBuild()
{
	int success = 0;
	int noArticles = ahObj->getSize();
	int rem = ahObj->getStatus();
	while (rem)
	{
		Page* pageObj = new Page(noPages, pageWidth, pageHeight);
		allPage[noPages] = pageObj->getContent(ahObj, 1);
		delete pageObj;
		success = 1;
		
		noPages++;
		rem = ahObj->getStatus();
	}
	getPageContent();
	exportNews();	
	return success;
}


int Newspaper::createPages()
{
	int success = 0;	// will be 1 if atleast one pages gets created and is the return value of this function
	int rem;
	int bol = 1;
	int noArticles = ahObj->getSize();
	Articlehandler* ahPrevious;

	rem = ahObj->getStatus();
	ahObj->printStatus();	// ** to be uncommented after integration

	char ch;
	while (bol)
	{
		cout<<"Press (C)ontinue or (Q)uit: ";
		cin>>ch;
		if ((ch == 'C') || (ch == 'c'))
		{
			Page* pageObj = new Page(noPages, pageWidth, pageHeight);
			allPage[noPages] = pageObj->getContent(ahObj);
			delete pageObj;
			success = 1;
			bol = 0;
		}
		else if ((ch == 'Q') || (ch == 'q'))
		{
			cout<<"Quitting Newspaper building without creating any page\n";
			cout<<"good bye...";
			exit(1);
		}
		else cout<<"invalid input! Try again\n";
	}
	bol = 1;
	if (success)
		while (bol)
		{
			cout<<"\n";
			rem = ahObj->getStatus();
			if (rem == 1)
			{
				cout<<"Press (C)ontinue, (B)uild again, (E)xport, (R)eset, (S)tatus, (V)iew or (Q)uit: ";
			}
			else 
			{
				cout<<"No article left to be written\n";
				cout<<"Press (R)eset, (E)xport, (S)tatus or (Q)uit: ";
			}
			cin>>ch;


			if ((ch == 'c') || (ch == 'C'))
			{
				rem = ahObj->getStatus();
//				cout<<"rem = "<<rem<<"\n";
				if (rem == 1)
				{
					// have to do a copy of the articlehandler in case of rebuild
					ahPrevious = new Articlehandler(ahObj);

					noPages++;
					Page* pageObj = new Page(noPages, pageWidth, pageHeight);
					allPage[noPages] = pageObj->getContent(ahObj);
					delete pageObj;
				}
				else cout<<"Invalid input! Try again.\n";
			}

			else if ((ch == 'b') || (ch == 'B'))
			{
				delete ahObj;
				ahObj = new Articlehandler(ahPrevious);
//				ahObj= new Articlehandler();
				
				Page* pageObj = new Page(noPages, pageWidth, pageHeight);
				allPage[noPages] = pageObj->getContent(ahObj);
				delete pageObj;
			}
			

			else if ((ch == 'r') || (ch == 'R'))						// line no 61
			{
//				delete ahObj;
//				ahObj = new Articlehandler();
//				Page* pageObj = new Page(noPages, pageWidth, pageHeight);
//				allPage[noPages] = pageObj->getContent(ahObj);
//				delete pageObj;
				
				resetAllPages();
			}

			else if ((ch == 's') || (ch == 'S'))
			{
				ahObj->printStatus();	// ** to be uncommented after integration
			}

			else if ((ch == 'e') || (ch == 'E'))
			{
				getPageContent();
				exportNews();
			}

			else if ((ch == 'v') || (ch == 'V'))
			{
				viewPage(allPage[noPages]);
			}

			else if ((ch == 'q') || (ch == 'Q'))
			{
				cout<<"Goodbye...\n";
				bol = 0;
			}
			else cout<<"Invalid input! Try again\n";
		}

	return success;
}

string Newspaper::getPageContent()
{
	pageContent = "";
	for (int i=0; i<=noPages; i++)
		pageContent = pageContent + allPage[i];
	return pageContent;
}

void Newspaper::exportNews()
{
	Exporter* exporter = new Exporter();
	
	string str;
	str = exporter->listFormats();
	cout << str << endl;

	int choice = 0;
	while ((choice != 1) && (choice != 2))
	{
		cout << "Your choice: ";
		cin >> choice;				// it hangs when char is inputted
		if ((choice != 1) && (choice != 2))
			cout<<"Invalid input! Type again\n";
	}

	string filename;
	cout << "Specify filename for output: ";
	cin >> filename;

//	cout<<pageContent;

	cout<<exporter->exportNewspaper(choice, filename, pageWidth, pageHeight, pageContent);
	cout<<endl;
}

void Newspaper::viewPage(string page)
{
	Exporter* exporter = new Exporter();	
	cout<<exporter->exportNewspaper(3, "viewpage.txt", pageWidth, pageHeight, page);
	cout<<endl;
}


void Newspaper::resetAllPages()
{
	cout<<"Resetting all pages\n";
	for (int i=0; i<noPages; i++)
		allPage[i] = "";		
	noPages = 0;
	delete ahObj;
	ahObj = new Articlehandler();
}
