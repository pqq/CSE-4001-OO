#include <iostream>
#include "template.h"
#include "exporter.h"
#include "articlehandler.h"
using namespace std;


// ---------------------------------------------------
// file:	picturePerfect.cpp
// author:	Frode Klevstul (frode@klevstul.com)
// started:	20030609
// ---------------------------------------------------


int main()
{
	Template* templ;
	Exporter* exporter;
	Articlehandler* articlehandler = new Articlehandler();

	int choice;
	int pagewidth = 80;
	int pageheight = 32;
	int pageno = 1;

	string pageContent;
	string str;
	string filename;
	string importances;
	string columns;
	
	cout << "*** test the Template (and Exporter) ***" << endl;


	// ------------------------
	// build a page
	// ------------------------

	cout << "Available templates:\n(1) Frontpage Template\n(2) Lastpage Template\n(3) Continue Template\n(4) Make your own template" << endl;

	cout << "Your choice: ";
	cin >> choice;
	
	if (choice==1)
		str = "FPAGTEMP";
	else if (choice==2)
		str = "LASTTEMP";
	else if (choice==3)
		str = "CONTTEMP";
	else if (choice==4){
		str = "CUSTTEMP";

		cout << "Specify the importances for this template (ex: 1,2,5): ";
		cin >> importances;
		
		cout << "Specify the number of columns for this template (max 10): ";
		cin >> columns;
		
		str+="-I:"+importances+"C:"+columns;
	}
	
	templ = Template::getTemplate(str);

	pageContent = templ->fetchArticles(pagewidth,pageheight,pageno, articlehandler);

	cout << "*** PAGE CONTENT ***\n" << pageContent << "\n*** /PAGE CONTENT ***" << endl;


	// -------------------------
	// export the page
	// -------------------------

	exporter = new Exporter();

	str = exporter->listFormats();
	
	cout << str << endl;

	cout << "Your choice: ";
	cin >> choice;
	cout << "Specify filename for output: ";
	cin >> filename;

	str = exporter->exportNewspaper(choice, filename, pagewidth, pageheight, pageContent);

	cout << str << endl;


}
