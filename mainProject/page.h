#ifndef PAGE_H
#define PAGE_H

#include <iostream>
#include <string>
#include "template.h"
#include "articlehandler.h"

class Page
{
	private:
		int width, height;
		int pageNumber;
		string pageContent;

	public:
		Page();
		Page(int pageno, int wdth, int high);
		~Page();
		string getContent(Articlehandler* ahObj);
		string getContent(Articlehandler* ahObj, int autobuild);
};
#endif
