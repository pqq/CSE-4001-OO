#ifndef LASTPAGETEMP_H_PROTECTOR
#define FRONTPAGETEMP_H_PROTECTOR

#include <iostream>
#include "template.h"
#include "articlehandler.h"
using namespace std;


// ---------------------------------------------------
// file:	lastpagetemp.h
// author:	Frode Klevstul (frode@klevstul.com)
// started:	20030611
// ---------------------------------------------------

class Lastpagetemp : public Template
{
    public:

		Lastpagetemp();

		virtual ~Lastpagetemp();
	
		string fetchArticles(int pagewidth, int pageheight, int pageno, Articlehandler* articlehandler) const;
		
	private:
	
		int _nocolumns;
		bool _importance[10];

};


#endif
