#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string.h>
#include "articlehandler.h"
#include <string>
using namespace std;

/*
Constructor for Articlehandler
class.It calls the setArticles()
method to initialise article
objects with inputs from file named
"File"
*/


Articlehandler::Articlehandler()
{
  setArticles();
}


/*  
Copy constructor
*/
Articlehandler::Articlehandler(Articlehandler* articlehandler)
{
	int i;
	setArticles();
	for(i=0; i<articlehandler->getSize(); i++){
		this->articles[i]->setBytes( articlehandler->articles[i]->getBytes() );
	}
}





/*

This method returns the total
number of articles

*/


int  Articlehandler::getSize(){

 char buf[1000];
 int counter = 0;
 ifstream fin;
 fin.open("input/articles.txt");
 while (fin.getline(buf,1000)){
     if (strstr(buf,"<HEADING>") != NULL){
      counter++;
     }
 }
 
 fin.close();
 return counter;

}


/* Each time when this function is called
*  it returns how many articles started,how
*  many of them finished,how many not yet
*  finished or how many not started yet.
*
*
*/
void Articlehandler::printStatus(){
 int i = 0;
 int k = 0;
 int t = 0;
 int p = 0;
 int q = 0;
 int r = 0; 
 int status[11] = {0};
 int articleStarted[11] = {0};
 int articleNotStarted[11] = {0};
 int articleFinished[11] = {0};
 string s = "";
 
 for ( i = 0; i < getSize(); i++){
 
  for (k =0; k < 11 ; k++){
  
    if ((*articles[i]).getImportance() == k){    
      status[k]++;
     
      if ((*articles[i]).getBytes() == 0){
        articleFinished[k]++;        
      }
    
      else if ((*articles[i]).getBytes() == (*articles[i]).getSize()){
        articleNotStarted[k]++;        
      }
   
      else {
        articleStarted[k]++; 
      }   
     }  
   }  
 }
 
for ( i = 0; i < getSize(); i++){
  
      if ((*articles[i]).getBytes() == 0){
        p++;        
      }
    
      else if ((*articles[i]).getBytes() == (*articles[i]).getSize()){
        q++;        
      }
   
      else {
        r++; 
      }   

}


cout << "There are total " << (q+r) << "/" << getSize() << " to be included" << endl;
cout << "No of articles finished :" << p << "," << "not yet finished :" << r <<
"," << " not yet started : " <<  q << endl;
 
 
 for ( t = 1; t < 11 ; t++){
 

cout << articleFinished[t]  << "/"  <<  status[t] << " art of imp "
<< t  << " " << "(" <<  articleFinished[t]   <<  " finished "  << ", " << articleStarted[t] << " started, " << articleNotStarted[t] << " not started" << ")" << endl;
   
 }
 

 
}

/*
* If still have any article
* not yet finished or included
* status is 1 ,otherwise 0
*/


int Articlehandler::getStatus(){

int i = 0;
int counter = 0;
 for ( i = 0; i < getSize(); i++){

  if ((*articles[i]).getBytes() == 0){
        counter++;        
      }


  }

 if ( counter == getSize()){
  return 0;
 }

 return 1;

}



/*
This method initialises article
objects taking inputs from file
named "File"
*/

void Articlehandler::setArticles(){

 int count = 0;
 char *body;
 char *heading;
 char *picture;
 int importance;
 char buf[1000];
 int size = 0;
 ifstream fin;
 fin.open("input/articles.txt");

 while (fin.getline(buf,1000)){
    
    if (strstr(buf,"<HEADING>") != NULL){
       fin.getline(buf,1000);
       heading =  new char[100]; 
       strcpy(heading,"");      
       while(strstr(buf,"</HEADING>") == NULL){
         strcat(heading,buf);
         fin.getline(buf,1000);
       }
    }
    
    if (strstr(buf,"<IMPORTANCE>") != NULL){
       fin.getline(buf,1000);
       while(strstr(buf,"</IMPORTANCE>") == NULL){
         importance = (int)atoi(buf);
         fin.getline(buf,1000);
       }
    }
    
    if (strstr(buf,"<PICTURE>") != NULL){ 
       fin.getline(buf,1000);
       picture =  new char[30];
       strcpy(picture,"");
       
       while(strstr(buf,"</PICTURE>") == NULL){
         strcat(picture,buf);
         fin.getline(buf,1000);
      }
    }
    
     if (strstr(buf,"<BODY>") != NULL){
       size = 0; 
       fin.getline(buf,1000);
       body =  new char[10000];
       strcpy(body,"");
       while(strstr(buf,"</BODY>") == NULL){
         size += strlen(buf)+1;
         strcat(body," ");
         strcat(body,buf);
         fin.getline(buf,1000);
       }
      
       articles[count] = new Article(heading,picture,body,importance,size,size);
       count++;
     
    }
   
 }

 fin.close();

}
