#ifndef NEWSPAPER_H
#define NEWSPAPER_H

#include <string>
#include "articlehandler.h"
#include "page.h"
#include "exporter.h"

using namespace std;

class Newspaper
{
	private:
		string name;
		int pageWidth;				// # of bytes
		int pageHeight;				// # of lines
		int noPages;				// # of pages the newspaper has
		string allPage[100];
		string pageContent;			// stores all the page content as single string 
		Articlehandler *ahObj;
		void resetAllPages();
	public:
		Newspaper(int);
		~Newspaper();

//		int getStatus();			// c if there is any article left, this function calls
							// getSize, printStatus method of Articlehandler class
		
		int autoBuild();			// Used for Auto building of newspaper
		int createPages();			// Used for Manual builing of newspaper
							// returns 1 if success or 0 if no pages gets created
		string getPageContent();		// it retrieves the content of the pages
		

		void exportNews();			// Export the paper to a particular format, calls the 
							// Exporter class's method
		void viewPage(string page);
};
#endif
