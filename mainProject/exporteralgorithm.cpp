#include <string>
#include "exporteralgorithm.h"
#include "export2html.h"
#include "export2ascii.h"

// ---------------------------------------------------
// file:	exporteralgorithm.cpp
// author:	Frode Klevstul (frode@klevstul.com)
// started:	20030606
// ---------------------------------------------------

ExporterAlgorithm* ExporterAlgorithm::getExporter(const string& name)
{
	if (name == "ASCII"){
		//cout << "DEBUG: Export2ASCII" << endl;
		Export2ASCII* exporter = new Export2ASCII();
		return exporter;
	} else if (name == "HTML") {
		//cout << "DEBUG: Export2HTML" << endl;
		Export2HTML* exporter = new Export2HTML();
		return exporter;	
	}

	return NULL;
}


// -------------------------------------------------------------
// returns the column WITHOUT "[column]" and "[/column]" tags
// -------------------------------------------------------------
string ExporterAlgorithm::getColumn(string& content) const {

	string str = "";

	int i = content.find("[column]");
	int y = content.find("[/column]");
	int length = content.length();

	if (i>=0 && y>=0){

		i += 8;
		y -= i;
		
		str = content.substr(i,y);
		
		y = content.find("[/column]");
		y += 9;
		content = content.substr(y, length);

	}

	return str;
}



bool ExporterAlgorithm::hasColumn(string& content) const {


	int i = content.find("[column]");
	if (i>=0)
		return true;
	else
		return false;

}


// -----------------------------------------------------------------------------------
// returns the page WITH "[page#xx]" and "[/page]" tags (because of the page number)
// -----------------------------------------------------------------------------------
string ExporterAlgorithm::getPage(string& content) const {
	string str = "";
	int i = content.find("[page#");
	int y = content.find("[/page]");

	if (i>=0 && y>=0){
		str = content.substr(i,y+7);
		content = content.substr(y+7, content.length());
	}

	return str;
}



bool ExporterAlgorithm::hasPage(string& content) const {

	int i = content.find("[page#");
	if (i>=0)
		return true;
	else
		return false;

}

int ExporterAlgorithm::getPagenumber(string page) const {

	string pageno = page;
	const char* c_pno;
	int pno;
	int i;

	i = pageno.find("[page#");
	if (i>=0){
		i+=6;
		pageno = pageno.substr(i,pageno.length());
		i = pageno.find("]");
		pageno = pageno.substr(0,i);
	} else {
		pageno = "000";
	}
	
	c_pno = pageno.c_str();
	pno = atoi(c_pno);

	return pno;
}


string ExporterAlgorithm::getArticle(string& content) const {

	string str = "";

	int i = content.find("[article#");
	int y = content.find("[/article]");
	int length = content.length();
	//cout << "[page] found in position: " << i << " and [/page] found in position " << y << endl;

	//cout << "content: " << content << endl << endl;

	if (i>=0 && y>=0){

		y += 10;
		
		str = content.substr(i,y);
	
		//cout << "return str: " << str << endl << endl;
	
		content = content.substr(y, length);
	
		//cout << "content: " << content << endl << endl;
	}


	return str;
}


bool ExporterAlgorithm::hasArticle(string& content) const {

	int i = content.find("[article#");
	if (i>=0)
		return true;
	else
		return false;

}


void ExporterAlgorithm::sustituteContTags(string& content) const{
	string artno;
	string pno1, pno2;
	int i,y,q,z;


	i = content.find("[continuing#-");
	while (i>=0){

		//step 1: get the article-id on the article that is continuing
		artno = content.substr(i+13,content.length());
		y = artno.find("]");
		artno = artno.substr(0,y);
				
		//step 2: get the pageno for this cont. tag
		q = content.substr(0,i).rfind("[page#");
		q+=6;
		pno1 = content.substr(q,content.length());
		q = pno1.find("]");
		pno1 = pno1.substr(0,q);

		//step 3: find the pagenumber where this article is continued
		z = content.find("[continued#-"+artno+"]");
		if (z<0)
			return;
		q = content.substr(0,z).rfind("[page#");
		q+=6;
		pno2 = content.substr(q,content.length());
		q = pno2.find("]");
		pno2 = pno2.substr(0,q);
		
		//step 4: replace the article id's with correct pagenumbers
		content.replace(i,14+artno.length(),"[continuing#"+pno2+"]");
		// note: have to subtract -1 the second time, since we remove one char in the first replace
		// and we have to subtract the differnce betwee the artno length and pageno length
		content.replace(z-1-(artno.length()-pno2.length()),13+artno.length(),"[continued#"+pno1+"]");
		
		i = content.find("[continuing#-");
	}
}



// -------------------------------------------------------------
// return the number of columns in a page
// -------------------------------------------------------------
int ExporterAlgorithm::getColNo(string page) const {
	int noCol = 0;
	int i;
	
	string str = "";

	i = page.find("[column]");
	while (i>=0){
		noCol++;
		page.replace(i,8,"");
		i = page.find("[column]");
	}

	return noCol;
}



