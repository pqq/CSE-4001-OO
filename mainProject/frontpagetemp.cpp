#include<iostream>
#include<string>
#include<sstream>
#include <stdio.h>

using namespace	std;
#include "frontpagetemp.h"


// ---------------------------------------------------
// file:	frontpagetemp.cpp
// author:	Frode Klevstul (frode@klevstul.com)
// started:	20030609
// ---------------------------------------------------


Frontpagetemp::Frontpagetemp(int maxsize){

	// define importance for this template
	_importance[1]=true;
	_importance[2]=true;
	_importance[3]=true;
	_importance[4]=false;
	_importance[5]=false;
	_importance[6]=false;
	_importance[7]=false;
	_importance[8]=false;
	_importance[9]=false;
	_importance[10]=false;

	// define the number of	columns for this template
	_nocolumns = 4;
	
	_maxsize = maxsize;
}


Frontpagetemp::~Frontpagetemp(){}



string Frontpagetemp::fetchArticles(int pagewidth, int pageheight, int pageno, Articlehandler* articlehandler) const{

	return fetchArticlesMethod(pagewidth, pageheight, pageno, _nocolumns, _importance, articlehandler, _maxsize);

}
