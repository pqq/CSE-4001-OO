#ifndef INTERFACE_H
#define INTERFACE_H

#include <iostream>
#include "newspaper.h"

class Interface
{
	private:
		int totalArticles;			// stores total number articles
		Newspaper* newsObj;
//		void showStatus();
	public:
		Interface();
		~Interface();
		void setupNewspaper();			// builds all the pages in Newspaper
		int buildPages();
};
#endif

