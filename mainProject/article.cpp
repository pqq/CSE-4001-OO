#include <iostream>
#include <cstring>
#include <cassert>
#include "article.h"

using namespace std;

/*
Constructor for Article class
*/
Article::Article(char *myHeading,char *myPicture,char *myContent,int myImportance,int mySize, int myBytesLeft,int myPageNo)
{

 heading = new char[strlen(myHeading)+1];
 assert(heading != 0);
 strcpy(heading,myHeading);
 
 picture = new char[strlen(myPicture)+1];
 assert(picture != 0);
 strcpy(picture,myPicture);
 
 content = new char[strlen(myContent)+1];
 assert(content != 0);
 strcpy(content,myContent);
 importance = myImportance;
 size = mySize;
 bytesLeft = myBytesLeft;
 pageNo = myPageNo;
}

/*

Destructor for Article class

*/

Article::~Article()
{

 delete [] heading;
 delete [] picture;
 delete [] content;

}

/*

This method returns the heading of the article


*/
char * Article::getHeading(){

 return heading;

} 

/*

This method returns the location of picture 
in the article 


*/

char * Article::getPicture(){

 return picture;

} 



char * Article::getContent() {

 return content;

} 


int Article::getImportance(){

 return importance;

}

int Article::getSize(){

 return size;

}


void Article::setBytes(int btes){

 bytesLeft = btes;
 
}

int  Article::getBytes(){

return  bytesLeft;

}
 
 
