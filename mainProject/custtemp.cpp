#include<iostream>
#include<string>
#include<sstream>
#include <stdio.h>
#include "custtemp.h"

using namespace	std;


// ---------------------------------------------------
// file:	custtemp.cpp
// author:	Frode Klevstul (frode@klevstul.com) 
// started:	20030612
// ---------------------------------------------------


Custtemp::Custtemp(bool importance [10], int nocolumns){
	int i;

	// didn't manage to do a copy, so have to do this...
	for (i=0; i<11; i++){
		_importance[i] = importance[i];
	}
	
	_nocolumns = nocolumns;
}


Custtemp::~Custtemp(){}


string Custtemp::fetchArticles(int pagewidth, int pageheight, int pageno, Articlehandler* articlehandler) const{

	return fetchArticlesMethod(pagewidth, pageheight, pageno, _nocolumns, _importance, articlehandler, 0);

}
