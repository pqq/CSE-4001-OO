#ifndef CUSTTEMP_H_PROTECTOR
#define CUSTTEMP_H_PROTECTOR

#include <iostream>
#include "template.h"
#include "articlehandler.h"
using namespace std;


// ---------------------------------------------------
// file:	custtemp.h
// author:	Frode Klevstul (frode@klevstul.com)
// started:	20030611
// ---------------------------------------------------

class Custtemp : public Template
{
    public:

		Custtemp(bool importance [10], int nocolumns);

		virtual ~Custtemp();
	
		string fetchArticles(int pagewidth, int pageheight, int pageno, Articlehandler* articlehandler) const;
		
	private:
	
		int _nocolumns;
		bool _importance[10];

};


#endif
