#ifndef CRYPTO_H_PROTECTOR
#define CRYPTO_H_PROTECTOR

#include <string>

using namespace std;

class Crypto
{
    public:
        /**
         * instantiates a new cryptographic object of the type specified in 'name'.
         * The object can then be used to encrypt and decrypt text using the 
         * algorithm specified in 'name' and the key specified in 'key'.
         * @param name the name of the cipher to use. Valid values are:
         * <ul><li><b>caesar</b> - Caesar Cipher</li>
         *     <li><b>monoalpha</b> - Monoalphabetic Cipher</li>
         *     <li><b>transposition</b> - Transposition Cipher</li>
         *     <li><b>cencryption</b> - Composite Encryption</li>
         * </ul>
         * @param key - the simmetric key to use for the encryption. 
         * @throws InvalidKeyException if the key is inappropriate for the encryption
         * scheme selected.
         * @return a cryptographic object allocated on the heap or NULL if the cipher
         * name provided is not one of the above. It is the user's responsibility 
         * to deallocate the object.
         */
        static Crypto* getCrypto(const string& name, const string& key);

        /**
         * Encrypts a plaintext message into a ciphertext.
         * @param m the plaintext message to encrypt
         * @return the ciphertext
         */
        virtual string encrypt(const string& m) const = 0;

        /**
         * Decrypts a ciphertext message into a plaintext
         * @param c the ciphertext message to decrypt
         * @return the plaintext
         */
        virtual string decrypt(const string& c) const = 0; 

};




#endif
