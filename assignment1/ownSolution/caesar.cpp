#include "caesar.h"

// --------------------------------------------------
// File:    caesar.cpp
// Author:  Frode Klevstul  (frode@klevstul.com)
// Started: 27.03 2003
// --------------------------------------------------




Caesar::Caesar(const string& key)
{
    if ( !validKey(key) )
        throw InvalidKeyException();

    _key = atoi(key.c_str());
}




Caesar::~Caesar()
{
}




string Caesar::encrypt(const string& m) const
{
    string encrypted_str = m;
    int i;
    int dec;

    for (i = 0; encrypted_str[i]!=0; i++)
    {
        encrypted_str[i] = toupper(encrypted_str[i]);   // to uppercase
        if ( (int)encrypted_str[i] == 32 )
            (int)encrypted_str[i] = 91;                 // changes 'space' (32) to '[' (91)
        dec = ((int)encrypted_str[i]-64+_key)%27;       // subtract 64 from the value (then A=1,B=2,...,Z=26, space=27), adds the key, and takes mod
        if (dec == 0)                                   // 27%27 = 0; we add a space (which has value 32)
            dec = 32;
        else
            dec += 64;

        encrypted_str[i] = (char)dec;
    }

    return encrypted_str;
}




string Caesar::decrypt(const string& m) const
{
    string decrypted_str = m;
    int i;
    int dec;

    for (i = 0; decrypted_str[i]!=0; i++)
    {
        decrypted_str[i] = toupper(decrypted_str[i]);   // to uppercase (every encoded string should be uppercase, but in case...)
        if ( (int)decrypted_str[i] == 32 )
            (int)decrypted_str[i] = 91;                 // changes 'space' (32) to '[' (91)
        dec = ((int)decrypted_str[i]-64-_key);          // subtract 64 from the value (then A=1,B=2,...,Z=26, space=27), subtracts the key

        if (dec < 0)
            dec = 91+dec;                               // subtracts (+-) the value of the char from the last position which is 91
        else if (dec != 0)                              // 'space' has now got the position 0, we don't want to change this
            dec += 64;

        decrypted_str[i] = (char)dec+32;
    }

    return decrypted_str;
}




bool Caesar::validKey(const string& key)
{
    int i;
    string s;
    
    
    for (i=0; key[i]!=0; i++)                       // goes through the string a checks if all characters are numbers
        if (!isdigit(key[i]))                       // if not all characters are digits
            return false;                           // it's a invalid key

    i = atoi(key.c_str());                          // we converts the string into a int
    
    if (i<1||i>26)                                  // the key is to big or to small
        return false;

    return true;                                    // key is valid
}
