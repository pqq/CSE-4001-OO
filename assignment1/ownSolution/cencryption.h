#ifndef CENCRYPTION_H_PROTECTOR
#define CENCRYPTION_H_PROTECTOR

#include <string>
#include <iostream>
#include "crypto.h"
#include "exception.h"

using namespace std;

// --------------------------------------------------
// File:    cencryption.h
// Author:  Frode Klevstul  (frode@klevstul.com)
// Started: 03.03 2003
// --------------------------------------------------




class CEncryption : public Crypto
{
    public:
        /**
         * Constructor
         * @param key the key used for encryption/decryption
         */
        CEncryption(const string& key);

        /**
         * Destructor
         * made virtual since parent class has virtual functions
         */
        virtual ~CEncryption();

        /**
         * Encrypts a text
         * @param m the string that is going to be encrypted
         * @return the encrypted text
         */
        string CEncryption::encrypt(const string& m) const;

        /**
         * Decrypts a text
         * @param m the string that is going to be decrypted
         * @return the decrypted text
         */
        string CEncryption::decrypt(const string& m) const;

    private:
        /**
         * Splits up the key and en-/decrypts parts of the string based on that key
         * @param str the string that is going to be decrypted 
         * @param type types that specifies encryption or decryption
         * @return the en-/decrypted text
         */
        string CEncryption::multiCryption(const string& str, const string& type) const;

        /**
         * The key used for encryption/decryption
         */
        string _key;

};




#endif
