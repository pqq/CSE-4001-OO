#ifndef MONOALPHA_H_PROTECTOR
#define MONOALPHA_H_PROTECTOR

#include <string>
#include <iostream>
#include "crypto.h"
#include "exception.h"

using namespace std;

// --------------------------------------------------
// File:    monoalpha.h
// Author:  Frode Klevstul  (frode@klevstul.com)
// Started: 28.03 2003
// --------------------------------------------------


class Monoalpha : public Crypto
{
    public:
        /**
         * Constructor
         * @param key the key used for encryption/decryption
         */
        Monoalpha(const string& key);

        /**
         * Destructor
         * made virtual since parent class has virtual functions
         */
        virtual ~Monoalpha();

        /**
         * Encrypts a text
         * @param m the string that is going to be encrypted
         * @return the encrypted text
         */
        string Monoalpha::encrypt(const string& m) const;

        /**
         * Decrypts a text
         * @param m the string that is going to be decrypted
         * @return the decrypted text
         */
        string Monoalpha::decrypt(const string& m) const;

    private:
        /**
         * Checks if a key is valid
         * @param key the key used for encoding/decoding
         * @return true: valid false: invalid
         */
        bool Monoalpha::validKey(const string& key);

        /**
         * The key used for encryption/decryption
         */
        string _key;

};




#endif
