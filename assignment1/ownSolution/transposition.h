#ifndef TRANSPOSITION_H_PROTECTOR
#define TRANSPOSITION_H_PROTECTOR

#include <string>
#include <iostream>
#include "crypto.h"
#include "exception.h"

using namespace std;


#define def_matrix_rows 100
#define def_matrix_cols 10

// --------------------------------------------------
// File:    transposition.h
// Author:  Frode Klevstul  (frode@klevstul.com)
// Started: 31.03 2003
// --------------------------------------------------


class Transposition : public Crypto
{
    public:
        /**
         * Constructor
         * @param key the key used for encryption/decryption
         */
        Transposition(const string& key);

        /**
         * Destructor
         * made virtual since parent class has virtual functions
         */
        virtual ~Transposition();

        /**
         * Encrypts a text
         * @param m the string that is going to be encrypted
         * @return the encrypted text
         */
        string Transposition::encrypt(const string& m) const;

        /**
         * Encrypts a text
         * @param m the string that is going to be encrypted
         * @return the encrypted text
         */
        string Transposition::decrypt(const string& m) const;

        /**
         * Initialize the matrix row by row (horizontal)
         * @param str the string to be inserted in matrix
         */
        void Transposition::initHorizontal(const string& str);

        /**
         * Initialize the matrix column by column (vertical)
         * @param str the string to be inserted in matrix
         */
        void Transposition::initVertical(const string& str);

        /**
         * Initialize the matrix column by column (vertical)
         * @return a string, reading the matrix vertical
         */
        string Transposition::getStringHorizontal();

        /**
         * Initialize the matrix column by column (horizontal)
         * @return a string, reading the matrix horizontal
         */
        string Transposition::getStringVertical();

        /**
         * Runs the key on the matrix, which results in shuffeling the columns
         */
        void Transposition::runKey();

    private:
        /**
         * Checks if a key is valid
         * @param key the key used for encoding/decoding
         * @return true: valid false: invalid
         */
        bool Transposition::validKey(const string& key);

        /**
         * help method (used for debugging), prints the matrix on screen
         */
        void Transposition::print();

        /**
         * Initialize the matrix verticaly or horizontaly, depends on the type
         * @param str the string to be inserted in matrix
         * @param type "horizontal" or "vertical", specifies how to initialize the matrix
         * @return a string, reading the matrix vertical
         */
        void Transposition::initialize(const string& str, const string& type);

        /**
         * Get a string from the matrix, reading row by row or col by col
         * @param type "horizontal" or "vertical", specifies how to read the matrix
         * @return the result of reading the matrix stored in a string
         */
        string Transposition::getString(const string& type);


        /**
         * _matrix[rows][columns]
         */
        char _matrix[def_matrix_rows][def_matrix_cols];

        /**
         * The number of columns in the matrix
         */
        int _cols;

        /**
         * The number of rows in the matrix
         */
        int _rows;

        /**
         * The key used for encryption/decryption
         */
        string _key;
};




#endif
