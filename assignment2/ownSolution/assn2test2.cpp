#include "ager.h"
#include <deque>

int main()
{
	Ager<int, deque<pair<size_t,int> > > ager(5);

	ager.push_back(10);
	ager.tick();
	ager.push_back(11);
	ager.tick();
	ager.push_back(12);
	ager.tick();
	ager.push_back(13);
	ager.push_back(14);
	ager.push_back(15);
	ager.tick();
	ager.pop_back();
	ager.pop_back();

	cout << "ager contains " << ager.size() << " elements:" << endl;
	for (size_t i = 0; i < ager.size() ; ++i)
		cout << ager[i] << endl;
	cout << endl;
	for (Ager<int, deque<pair<size_t,int> > >::reverse_iterator r = ager.rbegin(); r != ager.rend() ; ++r)
		cout << r.getAge() <<":"<< *r << endl;

	return EXIT_SUCCESS;
}

/** SHOULD PRINT: 

ager contains 4 elements:
10
11
12
13

1:13
2:12
3:11
4:10

*/
