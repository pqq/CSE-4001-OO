#ifndef TOGGLER_PROTECTOR
#define TOGGLER_PROTECTOR

#include <functional>
#include "prisoncell.h"


class Toggler : public unary_function<PrisonCell,void>{

	public:
		Toggler(int num) : _numerator(num), _cellpassed(1) { }
		
		void operator() (PrisonCell& pc){
			if ( _cellpassed%_numerator==0 )
				pc.toggle();
			_cellpassed++;
		}

	private:
		int _numerator;
		int _cellpassed;
};


#endif
