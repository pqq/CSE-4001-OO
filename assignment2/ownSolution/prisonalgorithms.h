#ifndef PRISONALGORITHMS_PROTECTOR
#define PRISONALGORITHMS_PROTECTOR

#include <iostream>


template <typename T>
size_t count_doors (T start, T end){
	size_t open_doors=0;
	while (start!=end){
		if ( start->isDoorOpen() )
			open_doors++;
		start++;
	}
	return open_doors;
}



template <typename T>
string xor_cells (T pc1Start, T pc1End, T pc2Start, T pc2End){
	string retString;

	while (pc2Start!=pc2End){
		if ( pc2Start->isDoorOpen() != pc1Start->isDoorOpen() ){
			retString += "+";
		} else {
			retString += " ";
		}
		pc1Start++;
		pc2Start++;
	}

	return retString;
}



template <typename T>
string show_cells (T start, T end){
	string retString;

	while (start!=end){
		if ( start->isDoorOpen() ){
			retString += "+";
		} else {
			retString += " ";
		}
		start++;
	}

	return retString;
}



#endif
